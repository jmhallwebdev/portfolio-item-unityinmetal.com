<?php

	class myEmail	{	
		  var $from;
          var $to;
          var $subject;
          var $body;

			// CONSTRUCTOR

          	function __construct($myEmails_from, $myEmails_to, $myEmails_subject, $myEmails_body) 
          	{
          		$this->from = $myEmails_from;
          		$this->to = $myEmails_to;
          		$this->subject = $myEmails_subject;
          		$this->body = $myEmails_body;
          	}

          	//SETTERS START HERE

 			function setFrom($inFrom) 
				{
				$this->from = $inFrom;
				}			
			function setTo($inTo) 
				{
				$this->to = $inTo;
				}	
			function setSubject($inSubject) 
				{
				$this->subject = $inSubject;
				}	
			function setBody($inBody) 
				{
				$this->body = $inBody;
				}	

			// GETTERS START HERE
			
			function getFrom() 
				{
				return $this->from;
				}			
			function getTo() 
				{
				return $this->to;
				}	
			function getSubject() 
				{
				return $this->subject;
				}	
			function getBody() 
				{
				return $this->body;
				}	

			// ADDITIONAL METHODS START HERE

			function sendEmail()	
				{
				mail($this->to, $this->subject, $this->body, $this->from);
				}	
				
	}	//end Product class
?>